'use strict';

const uuid = require('uuid');

exports.seed = (knex, Promise) => {
  return Promise.join(
    knex('users').insert({
      id: uuid.v4(),
      name: 'Delobel',
      firstname: 'Julien',
      login: 'drexlbob',
      email: 'drexlbob@gmail.com',
      created_at: new Date()
    }),
    knex('users').insert({
      id: uuid.v4(),
      name: 'Guiot',
      firstname: 'Ghislain',
      login: 'gguiot',
      email: 'gguiot@gmail.com',
      created_at: new Date()
    })
  );
};
