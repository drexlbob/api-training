# api-training - step 1


# Prerequisite

- Install npm > 2.0.0
- Install node >= 4.0.0
- Install docker on your machine https://docs.docker.com/engine/installation/

#Installation

## Clone the repository
```bash
git clone git@gitlab:drexlbob/api-training#step1.git
```

## Install project dependencies
Install dependencies
```bash
npm install
```

## Run PostgreSQL DOcker container
```bash
docker run \
  --name postgresql-api-training \
  -it \
  -p 5432:5432 \
  -e POSTGRES_USER=api-training \
  -e POSTGRES_PASSWORD=api-training \
  -e POSTGRES_DB=api-training \
  -d --restart=always  postgres
```

# Get started

If you open `package.json` you will see knex and bookshelf dependencies. This dependencies is used to interract with the database.

## Knex
The `knexfile.js` is the entrypoint to connect your api to the PostgreSQL Database. (host, user, password, migrations , seeds...).

### Migrations

Migrations files is used to populate the schema database. In **step 1**, one migration file already exists into `migrations` directory. This file is used to create the `users` table

To create `users` table run:
```bash
knex migrate:latest
```

To rollback the latest migration run:
```bash
knex migrate:rollback # delete users table
```

**DO IT**: Now create a new migration file and add `skills` table into it:
```bash
knex migrate:make add_skills # this line generate a new file in migration directory
knex migrate:latest
```

**DO IT**: This file is responsible for creating a table `skills` with a `name`, `created_at` and `updated_at` properties. **`name` must be not null and must have an unique constraint**.

Now we have 2 tables:
-  users
- skills

### Seeds

```bash
knex seed:run # populate database
```
Note: Seeds are generally used to populate database for testing purpose.

**DO IT**: Now create a new migration file `skills.js` in `seeds` directory. This file is responsible for populate the existing table `skills` with pre-defined data.

then re-run:
```bash
knex seeds:run
```

### Querying

Knex is a powerfull query builder. You can edit the index.js file and add it:
```js
function getUsers() {
  return knex.raw('SELECT * from users')
    .then(users => users.rows)
    .catch(err => console.error(err));
}
```
Run the fallowing commmand to see the results:
```bash
npm start # log array of users from database
```

**DO IT**: Now add your function getSkills() and log its result.

## Bookshelf

Now we will add top level ORM Bookshelf built on the  Knex SQL query builder.

First, we have to create models:

Create model directory and add the fallowing file:

- users.js

```js
'use strict';

const connection = require('../knexfile').development;
const knex = require('knex')(connection);
const bookshelf = require('bookshelf')(knex);

module.exports = bookshelf.Model.extend({
  tableName: 'users'
});
```

**DO IT**: Now create a file `skills.js` that contains its model

Now replace `index.js` content with it:
```js
'use strict';

const User = require('./models/users');
function getUsers() {
  return User.fetchAll();
}
const users = getUsers();

getUsers().then(users => console.log(users.toJSON()));
```

**DO IT**: Now re-create your function getSkills() that call `Skill` model and log its result.

Congratulations you have finished the step 1!
