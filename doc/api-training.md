# api-training

Learn basics of PostgreSQL, Knex.js, Bookshelf.js and restify to expose CRUD data through Node.js RESTful API

## PostgreSQL
- SGBDRO
- Debut des années 90
- Initié à Berkeley par Michael Stonebraker
- SQL

### Outils d'administration
- psql (CLI)
- pgAdmin (Client lourd qui plante tout le temps)
- phpPgAdmin (Client léger)

More info: https://fr.wikipedia.org/wiki/PostgreSQL


---

## Knex.js
-  Initié à Brooklyn par Tim Griesser
- SQL query buidler  for PostgreSQL, MySQL and SQLite3, WebSQL, Oracle

Github: https://github.com/tgriesser/
Doc: http://knexjs.org/

---

## Bookshelf.js

- A simple Node.js ORM for PostgreSQL, MySQL and SQLite3 built on top of Knex.js

Github: https://github.com/tgriesser/bookshelf
Doc: http://bookshelfjs.org/

---

## restify
- Node module to build REST service

- express without templating and rendering
Doc: http://restify.com/
Github: https://github.com/restify/node-restify

