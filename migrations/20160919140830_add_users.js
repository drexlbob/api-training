'use strict';

exports.up = (knex, Promise) => {
  return knex.schema
    .createTable('users', (table) => {
      table.uuid('id').primary().notNullable();
      table.string('name').notNullable();
      table.string('firstname').notNullable();
      table.string('login').unique().notNullable();
      table.string('email').unique().notNullable();
      table.datetime('created_at').notNullable().defaultTo(knex.raw('now()'));
      table.datetime('updated_at');
    });
};

exports.down = (knex, Promise) => {
  return knex.schema.dropTableIfExists('users');
};
