'use strict';

module.exports = {
  development: {
    client: 'pg',
    connection: {
      host: '127.0.0.1',
      user: 'api-training',
      password: 'api-training',
      database: 'api-training',
      charset: 'utf8'
    },
    pool: {
      min: 0,
      max: 25
    },
    debug: false
  }
};
